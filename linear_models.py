# %%
import numpy as np
import matplotlib.pyplot as plt

# Initialize random number generator
np.random.seed(123)

# True parameter values
alpha, sigma = 1, 3
beta = 1

# Size of dataset
size = 100

# Predictor variable
X1 = np.random.randn(size)

# Simulate outcome variable
Y = alpha + beta*X1 + np.random.randn(size)*sigma
#%%
plt.figure()
plt.scatter(X1, Y)
plt.ylabel('Y')
plt.xlabel('X1')
plt.show()
#%%
import pymc3 as pm
basic_model = pm.Model()

with basic_model:

    # Priors for unknown model parameters
    alpha = pm.Normal('alpha', mu=0, sd=50)
    beta = pm.Uniform('beta', lower=0, upper=10)
    sigma = pm.Uniform('sigma', lower=0, upper=50)

    # Expected value of outcome
    mu = alpha + beta*X1

    # Likelihood (sampling distribution) of observations
    Y_obs = pm.Normal('Y_obs', mu=mu, sd=sigma, observed=Y)

#%%
with basic_model:

    # instantiate sampler
    # step = pm.Slice()

    # draw 5000 posterior samples
    trace = pm.sample(5000)

#%%
pm.traceplot(trace)