# %%
import numpy as np
import matplotlib.pyplot as plt
import pandas as pd 

# %%
df = pd.read_csv('Howell1.csv', sep=';')

#%%
def plot_scatter(df):
    plt.figure(figsize=(10,10))
    plt.scatter(df['weight'], df['height'])
    plt.xlabel('weight')
    plt.ylabel('height')
    plt.show()
#%%
# I split data into 3 datasets (for different age intervals)

Y = [df[df.age >= 18], 
     df[df.age >= 55], 
     df[(df.age >= 10) & (df.age < 18)]]

for data in Y:
    plot_scatter(data)
#%%
import pymc3 as pm
basic_model = pm.Model()

with basic_model:
    # My prior assumtions 
    mu = pm.Normal('mu', 178, 20) 
    sigma = pm.Uniform('sigma', lower=0, upper=50)
    # 95% of probability between 178 ± 40
    height = pm.Normal('height', mu, sigma, observed=Y[2]['height']) # likelihood

    
#%%
with basic_model:
    trace = pm.sample(5000)

pm.summary(trace)
pm.traceplot(trace)

trace_df = pm.trace_to_dataframe(trace)
# covariance matrix
trace_df.cov()
#%%
# We got Gaussian model of height in a population of adults. Now it's time for regression.
# Let's create linear model.
#%%
linear_model = pm.Model()
with linear_model:

    # Priors for unknown model parameters
    alpha = pm.Normal('Intercept', mu=178, sd=50)
    beta = pm.Uniform('x', lower=0, upper=10)
    sigma = pm.Uniform('sigma', lower=0, upper=50)

    # Expected value of outcome
    mu = alpha + beta*Y[2].weight

    # Likelihood (sampling distribution) of observations
    height = pm.Normal('height', mu=mu, sd=sigma, observed=Y[2].height)

#%%
with linear_model:
    # draw 5000 posterior samples
    trace = pm.sample(5000)

#%%
with linear_model:
    plt.figure(figsize=(7, 7))
    plt.plot(Y[2].weight, Y[2].height, 'x', label='data')
    pm.plot_posterior_predictive_glm(trace, samples=100, eval=np.linspace(10,50,100),
                                    label='posterior predictive regression lines')
    plt.title('Posterior predictive regression lines')
    plt.legend(loc=0)
    plt.xlabel('x')
    plt.ylabel('y')

#%%
#Logaritmic
log_model = pm.Model()

with log_model:
    # Priors for unknown model parameters
    alpha = pm.Normal('Intercept', mu=178, sd=100)
    beta = pm.Normal('x', mu=0, sd=100)
    sigma = pm.Uniform('sigma', lower=0, upper=50)

    # Expected value of outcome
    mu = alpha + beta*np.log(df.weight)

    # Likelihood (sampling distribution) of observations
    height = pm.Normal('height', mu=mu, sd=sigma, observed=df.height)
#%%
with log_model:
    # draw 5000 posterior samples
    trace = pm.sample(5000)

#%%
with log_model:
    plt.figure(figsize=(7, 7))
    plt.plot(Y[2].weight, Y[2].height, 'x', label='data')
    pm.plot_posterior_predictive_glm(trace, samples=100)
    plt.title('Posterior predictive regression lines')
    plt.legend(loc=0)
    plt.xlabel('x')
    plt.ylabel('y')